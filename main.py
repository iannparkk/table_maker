from prettytable import PrettyTable
from time import sleep
import os


print("""
 +--------+--------+
 | Create |   A    |
 +--------+--------+
 | Table  | Easily |
 +--------+--------+""")

def section_count_1():
    List_1 = input(' List Name: ')
    List1 = []

    amount = int(input(' How many objects: '))
    count = 0
    for i in range(amount):
        count += 1
        values = input(' ' + List_1 + ' ' + str(count) + ': ')
        List1.append(values)

    table = PrettyTable([(List_1)])
    for i in range(amount):
        table.add_row([List1[i]])

    title = input('\n File name you would like to save it as: ')
    paths = input(' The PATH you would like to save it to: ')
    title = title.replace(' ', '-')
    area = os.path.join(paths, title + '.txt')
    with open(area, 'w') as f:
        f.write(str(table))
    print('\n')
    print('-----------------------------')
    print(' Output saved to \'' + title + '.txt\'')
    print('-----------------------------')
    input(' Press [Enter] to exit')
def section_count_2():
    List_1 = input(' Comparison 1: ')
    List_2 = input(' Comparison 2: ')
    List1 = []
    List2 = []

    amount = int(input(' How many objects: '))
    print('\n(You\'ll list out all the objects for each Comparison at a time)')
    count = 0
    for i in range(amount):
        count += 1
        values = input(' ' + List_1 + ' ' + str(count) + ': ')
        List1.append(values)
    print('\nMOVING ')
    sleep(2)
    count = 0
    print('\n (The Previous word is to help you correlate what you are comparing)')
    for words in List1:
        print(' Previous word:', words)
        values = input(' ' + List_2 + ' > ')
        List2.append(values)

    table = PrettyTable([(List_1),(List_2)])
    for i in range(amount):
        table.add_row([List1[i], List2[i]])

    title = input('\n File name you would like to save it as: ')
    paths = input(' The PATH you would like to save it to: ')
    title = title.replace(' ', '-')
    area = os.path.join(paths, title + '.txt')
    with open(area, 'w') as f:
        f.write(str(table))
    print('\n')
    print('-----------------------------')
    print(' Output saved to \'' + title + '.txt\'')
    print('-----------------------------')
    input(' Press [Enter] to exit')

def section_count_3():
    List_1 = input(' Comparison 1: ')
    List_2 = input(' Comparison 2: ')
    List_3 = input(' Comparison 3: ')
    List1 = []
    List2 = []
    List3 = []

    amount = int(input(' How many objects: '))
    print('\n(You\'ll list out all the objects for each Comparison at a time)')
    count = 0
    for i in range(amount):
        count += 1
        values = input(' ' + List_1 + ' ' + str(count) + ': ')
        List1.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    count = 0
    print('\n (The Previous word is to help you correlate what you are comparing)')
    for words in List1:
        print(' Previous word:', words)
        values = input(' ' + List_2 + ' > ')
        List2.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List2:
        print(' Previous word:', words)
        values = input(' ' + List_3 + ' > ')
        List3.append(values)

    table = PrettyTable([(List_1),(List_2),(List_3)])
    for i in range(amount):
        table.add_row([List1[i], List2[i], List3[i]])

    title = input('\n File name you would like to save it as: ')
    paths = input(' The PATH you would like to save it to: ')
    title = title.replace(' ', '-')
    area = os.path.join(paths, title + '.txt')
    with open(area, 'w') as f:
        f.write(str(table))
    print('\n')
    print('-----------------------------')
    print(' Output saved to \'' + title + '.txt\'')
    print('-----------------------------')
    input(' Press [Enter] to exit')

def section_count_4():
    List_1 = input(' Comparison 1: ')
    List_2 = input(' Comparison 2: ')
    List_3 = input(' Comparison 3: ')
    List_4 = input(' Comparison 4: ')
    List1 = []
    List2 = []
    List3 = []
    List4 = []

    amount = int(input(' How many objects: '))
    print('\n(You\'ll list out all the objects for each Comparison at a time)')
    count = 0
    for i in range(amount):
        count += 1
        values = input(' ' + List_1 + ' ' + str(count) + ': ')
        List1.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    count = 0
    print('\n (The Previous word is to help you correlate what you are comparing)')
    for words in List1:
        print(' Previous word:', words)
        values = input(' ' + List_2 + ' > ')
        List2.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List2:
        print(' Previous word:', words)
        values = input(' ' + List_3 + ' > ')
        List3.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List3:
        print(' Previous word:', words)
        values = input(' ' + List_4 + ' > ')
        List4.append(values)

    table = PrettyTable([(List_1),(List_2),(List_3),(List_4)])
    for i in range(amount):
        table.add_row([List1[i], List2[i], List3[i], List4[i]])

    title = input('\n File name you would like to save it as: ')
    paths = input(' The PATH you would like to save it to: ')
    title = title.replace(' ', '-')
    area = os.path.join(paths, title + '.txt')
    with open(area, 'w') as f:
        f.write(str(table))
    print('\n')
    print('-----------------------------')
    print(' Output saved to \'' + title + '.txt\'')
    print('-----------------------------')
    input(' Press [Enter] to exit')

def section_count_5():
    List_1 = input(' Comparison 1: ')
    List_2 = input(' Comparison 2: ')
    List_3 = input(' Comparison 3: ')
    List_4 = input(' Comparison 4: ')
    List_5 = input(' Comparison 5: ')
    List1 = []
    List2 = []
    List3 = []
    List4 = []
    List5 = []

    amount = int(input(' How many objects: '))
    print('\n(You\'ll list out all the objects for each Comparison at a time)')
    count = 0
    for i in range(amount):
        count += 1
        values = input(' ' + List_1 + ' ' + str(count) + ': ')
        List1.append(values)
    print('MOVING LISTS')
    sleep(1)
    count = 0
    print('\n (The Previous word is to help you correlate what you are comparing)')
    for words in List1:
        print(' Previous word:', words)
        values = input(' ' + List_2 + ' > ')
        List2.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List2:
        print(' Previous word:', words)
        values = input(' ' + List_3 + ' > ')
        List3.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List3:
        print(' Previous word:', words)
        values = input(' ' + List_4 + ' > ')
        List4.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List4:
        print(' Previous word:', words)
        values = input(' ' + List_5 + ' > ')
        List5.append(values)

    table = PrettyTable([(List_1),(List_2),(List_3),(List_4),(List_5)])
    for i in range(amount):
        table.add_row([List1[i], List2[i], List3[i], List4[i], List5[i]])

    title = input('\n File name you would like to save it as: ')
    paths = input(' The PATH you would like to save it to: ')
    title = title.replace(' ', '-')
    area = os.path.join(paths, title + '.txt')
    with open(area, 'w') as f:
        f.write(str(table))
    print('\n')
    print('-----------------------------')
    print(' Output saved to \'' + title + '.txt\'')
    print('-----------------------------')
    input(' Press [Enter] to exit')

def section_count_6():
    List_1 = input(' Comparison 1: ')
    List_2 = input(' Comparison 2: ')
    List_3 = input(' Comparison 3: ')
    List_4 = input(' Comparison 4: ')
    List_5 = input(' Comparison 5: ')
    List_6 = input(' Comparison 6: ')
    List1 = []
    List2 = []
    List3 = []
    List4 = []
    List5 = []
    List6 = []

    amount = int(input(' How many objects: '))
    print('\n(You\'ll list out all the objects for each Comparison at a time)')
    count = 0
    for i in range(amount):
        count += 1
        values = input(' ' + List_1 + ' ' + str(count) + ': ')
        List1.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    count = 0
    print('\n (The Previous word is to help you correlate what you are comparing)')
    for words in List1:
        print(' Previous word:', words)
        values = input(' ' + List_2 + ' > ')
        List2.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List2:
        print(' Previous word:', words)
        values = input(' ' + List_3 + ' > ')
        List3.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List3:
        print(' Previous word:', words)
        values = input(' ' + List_4 + ' > ')
        List4.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List4:
        print(' Previous word:', words)
        values = input(' ' + List_5 + ' > ')
        List5.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List5:
        print(' Previous word:', words)
        values = input(' ' + List_6 + ' > ')
        List6.append(values)

    table = PrettyTable([(List_1),(List_2),(List_3),(List_4),(List_5),(List_6)])
    for i in range(amount):
        table.add_row([List1[i], List2[i], List3[i], List4[i], List5[i], List6[i]])

    title = input('\n File name you would like to save it as: ')
    paths = input(' The PATH you would like to save it to: ')
    title = title.replace(' ', '-')
    area = os.path.join(paths, title + '.txt')
    with open(area, 'w') as f:
        f.write(str(table))
    print('\n')
    print('-----------------------------')
    print(' Output saved to \'' + title + '.txt\'')
    print('-----------------------------')
    input(' Press [Enter] to exit')

def section_count_7():
    List_1 = input(' Comparison 1: ')
    List_2 = input(' Comparison 2: ')
    List_3 = input(' Comparison 3: ')
    List_4 = input(' Comparison 4: ')
    List_5 = input(' Comparison 5: ')
    List_6 = input(' Comparison 6: ')
    List_7 = input(' Comparison 7: ')
    List1 = []
    List2 = []
    List3 = []
    List4 = []
    List5 = []
    List6 = []
    List7 = []

    amount = int(input(' How many objects: '))
    print('\n(You\'ll list out all the objects for each Comparison at a time)')
    count = 0
    for i in range(amount):
        count += 1
        values = input(' ' + List_1 + ' ' + str(count) + ': ')
        List1.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    count = 0
    print('\n (The Previous word is to help you correlate what you are comparing)')
    for words in List1:
        print(' Previous word:', words)
        values = input(' ' + List_2 + ' > ')
        List2.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List2:
        print(' Previous word:', words)
        values = input(' ' + List_3 + ' > ')
        List3.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List3:
        print(' Previous word:', words)
        values = input(' ' + List_4 + ' > ')
        List4.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List4:
        print(' Previous word:', words)
        values = input(' ' + List_5 + ' > ')
        List5.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List5:
        print(' Previous word:', words)
        values = input(' ' + List_6 + ' > ')
        List6.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List6:
        print(' Previous word:', words)
        values = input(' ' + List_7 + ' > ')
        List7.append(values)

    table = PrettyTable([(List_1),(List_2),(List_3),(List_4),(List_5),(List_6),(List_7)])
    for i in range(amount):
        table.add_row([List1[i], List2[i], List3[i], List4[i], List5[i], List6[i], List7[i]])

    title = input('\n File name you would like to save it as: ')
    paths = input(' The PATH you would like to save it to: ')
    title = title.replace(' ', '-')
    area = os.path.join(paths, title + '.txt')
    with open(area, 'w') as f:
        f.write(str(table))
    print('\n')
    print('-----------------------------')
    print(' Output saved to \'' + title + '.txt\'')
    print('-----------------------------')
    input(' Press [Enter] to exit')

def section_count_8():
    List_1 = input(' Comparison 1: ')
    List_2 = input(' Comparison 2: ')
    List_3 = input(' Comparison 3: ')
    List_4 = input(' Comparison 4: ')
    List_5 = input(' Comparison 5: ')
    List_6 = input(' Comparison 6: ')
    List_7 = input(' Comparison 7: ')
    List_8 = input(' Comparison 8: ')
    List1 = []
    List2 = []
    List3 = []
    List4 = []
    List5 = []
    List6 = []
    List7 = []
    List8 = []

    amount = int(input(' How many objects: '))
    print('\n(You\'ll list out all the objects for each Comparison at a time)')
    count = 0
    for i in range(amount):
        count += 1
        values = input(' ' + List_1 + ' ' + str(count) + ': ')
        List1.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    count = 0
    print('\n (The Previous word is to help you correlate what you are comparing)')
    for words in List1:
        print(' Previous word:', words)
        values = input(' ' + List_2 + ' > ')
        List2.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List2:
        print(' Previous word:', words)
        values = input(' ' + List_3 + ' > ')
        List3.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List3:
        print(' Previous word:', words)
        values = input(' ' + List_4 + ' > ')
        List4.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List4:
        print(' Previous word:', words)
        values = input(' ' + List_5 + ' > ')
        List5.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List5:
        print(' Previous word:', words)
        values = input(' ' + List_6 + ' > ')
        List6.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List6:
        print(' Previous word:', words)
        values = input(' ' + List_7 + ' > ')
        List7.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List7:
        print(' Previous word:', words)
        values = input(' ' + List_8 + ' > ')
        List8.append(values)

    table = PrettyTable([(List_1),(List_2),(List_3),(List_4),(List_5),(List_6),(List_7)],(List_8))
    for i in range(amount):
        table.add_row([List1[i], List2[i], List3[i], List4[i], List5[i], List6[i], List7[i], List8[i]])

    title = input('\n File name you would like to save it as: ')
    paths = input(' The PATH you would like to save it to: ')
    title = title.replace(' ', '-')
    area = os.path.join(paths, title + '.txt')
    with open(area, 'w') as f:
        f.write(str(table))
    print('\n')
    print('-----------------------------')
    print(' Output saved to \'' + title + '.txt\'')
    print('-----------------------------')
    input(' Press [Enter] to exit')

def section_count_9():
    List_1 = input(' Comparison 1: ')
    List_2 = input(' Comparison 2: ')
    List_3 = input(' Comparison 3: ')
    List_4 = input(' Comparison 4: ')
    List_5 = input(' Comparison 5: ')
    List_6 = input(' Comparison 6: ')
    List_7 = input(' Comparison 7: ')
    List_8 = input(' Comparison 8: ')
    List_9 = input(' Comparison 9: ')
    List1 = []
    List2 = []
    List3 = []
    List4 = []
    List5 = []
    List6 = []
    List7 = []
    List8 = []
    List9 = []

    amount = int(input(' How many objects: '))
    print('\n(You\'ll list out all the objects for each Comparison at a time)')
    count = 0
    for i in range(amount):
        count += 1
        values = input(' ' + List_1 + ' ' + str(count) + ': ')
        List1.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    count = 0
    print('\n (The Previous word is to help you correlate what you are comparing)')
    for words in List1:
        print(' Previous word:', words)
        values = input(' ' + List_2 + ' > ')
        List2.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List2:
        print(' Previous word:', words)
        values = input(' ' + List_3 + ' > ')
        List3.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List3:
        print(' Previous word:', words)
        values = input(' ' + List_4 + ' > ')
        List4.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List4:
        print(' Previous word:', words)
        values = input(' ' + List_5 + ' > ')
        List5.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List5:
        print(' Previous word:', words)
        values = input(' ' + List_6 + ' > ')
        List6.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List6:
        print(' Previous word:', words)
        values = input(' ' + List_7 + ' > ')
        List7.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List7:
        print(' Previous word:', words)
        values = input(' ' + List_8 + ' > ')
        List8.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List8:
        print(' Previous word:', words)
        values = input(' ' + List_9 + ' > ')
        List9.append(values)

    table = PrettyTable([(List_1),(List_2),(List_3),(List_4),(List_5),(List_6),(List_7)],(List_8),(List_9))
    for i in range(amount):
        table.add_row([List1[i], List2[i], List3[i], List4[i], List5[i], List6[i], List7[i], List8[i], List9[i]])

    title = input('\n File name you would like to save it as: ')
    paths = input(' The PATH you would like to save it to: ')
    title = title.replace(' ', '-')
    area = os.path.join(paths, title + '.txt')
    with open(area, 'w') as f:
        f.write(str(table))
    print('\n')
    print('-----------------------------')
    print(' Output saved to \'' + title + '.txt\'')
    print('-----------------------------')
    input(' Press [Enter] to exit')

def section_count_10():
    List_1 = input(' Comparison 1: ')
    List_2 = input(' Comparison 2: ')
    List_3 = input(' Comparison 3: ')
    List_4 = input(' Comparison 4: ')
    List_5 = input(' Comparison 5: ')
    List_6 = input(' Comparison 6: ')
    List_7 = input(' Comparison 7: ')
    List_8 = input(' Comparison 8: ')
    List_9 = input(' Comparison 9: ')
    List_10 = input(' Comparison 10: ')
    List1 = []
    List2 = []
    List3 = []
    List4 = []
    List5 = []
    List6 = []
    List7 = []
    List8 = []
    List9 = []
    List10 = []

    amount = int(input(' How many objects: '))
    print('\n(You\'ll list out all the objects for each Comparison at a time)')
    count = 0
    for i in range(amount):
        count += 1
        values = input(' ' + List_1 + ' ' + str(count) + ': ')
        List1.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    count = 0
    print('\n (The Previous word is to help you correlate what you are comparing)')
    for words in List1:
        print(' Previous word:', words)
        values = input(' ' + List_2 + ' > ')
        List2.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List2:
        print(' Previous word:', words)
        values = input(' ' + List_3 + ' > ')
        List3.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List3:
        print(' Previous word:', words)
        values = input(' ' + List_4 + ' > ')
        List4.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List4:
        print(' Previous word:', words)
        values = input(' ' + List_5 + ' > ')
        List5.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List5:
        print(' Previous word:', words)
        values = input(' ' + List_6 + ' > ')
        List6.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List6:
        print(' Previous word:', words)
        values = input(' ' + List_7 + ' > ')
        List7.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List7:
        print(' Previous word:', words)
        values = input(' ' + List_8 + ' > ')
        List8.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List8:
        print(' Previous word:', words)
        values = input(' ' + List_9 + ' > ')
        List9.append(values)
    print('\nMOVING LISTS')
    sleep(1)
    for words in List9:
        print(' Previous word:', words)
        values = input(' ' + List_10 + ' > ')
        List10.append(values)

    table = PrettyTable([(List_1),(List_2),(List_3),(List_4),(List_5),(List_6),(List_7),(List_8),(List_9),(List_10)])
    for i in range(amount):
        table.add_row([List1[i], List2[i], List3[i], List4[i], List5[i], List6[i], List7[i], List8[i], List9[i], List10[i]])

    title = input('\n File name you would like to save it as: ')
    paths = input(' The PATH you would like to save it to: ')
    title = title.replace(' ', '-')
    area = os.path.join(paths, title + '.txt')
    with open(area, 'w') as f:
        f.write(str(table))
    print('\n')
    print('-----------------------------')
    print(' Output saved to \'' + title + '.txt\'')
    print('-----------------------------')
    input(' Press [Enter] to exit')

def main():
    print('\n How Many Sections Do You Need?')
    print(' [1]\t[6]\n [2]\t[7]\n [3]\t[8]\n [4]\t[9]\n [5]\t[10]')
    answer = True
    while answer == True:
        response = input('\n $ ')
        if response == '1':
            section_count_1()
            answer == False
        elif response == '2':
            section_count_2()
            answer == False
        elif response == '3':
            section_count_3()
            answer == False
        elif response == '4':
            section_count_4()
            answer == False
        elif response == '5':
            section_count_5()
            answer == False
        elif response == '6':
            section_count_6()
            answer == False
        elif response == '7':
            section_count_7()
            answer == False
        elif response == '8':
            section_count_8()
            answer == False
        elif response == '9':
            section_count_9()
            answer == False
        elif response == '10':
            section_count_10()
            answer == False
        else:
            answer == False

main()
